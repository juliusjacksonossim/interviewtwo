import React, { useState} from 'react';

import { Button, Card, CardBody, CardTitle, CardText} from 'reactstrap';
import {description} from "./description";
import './App.css';
import TopBar from "./Navbar";
import Question1 from "./Modal";
import Question2 from "./Table";
function App() {
    const text = description.substr(0, 50);
    const [modal, setModal] = useState(false);
    const [entered, setEntered] = useState(false);
    const [show, setShow] = useState(false);
    const launchModal = ()=>setModal(true) ;
  return (
    <div className='container-fluid'>
      <TopBar/>
      <div className='row'>
        <div className="offset-md-2 col-md-8">
            <div className='mb-5 mt-5'>
                <Card>
                    <CardBody>
                        <CardTitle className='text-info'>Question One Solution</CardTitle>
                        <CardText>Some Texts were added to demonstrate all the constraints.</CardText>
                        <Button color={'primary'} onClick={()=>launchModal()}>Launch Modal</Button>
                        <Question1 show={modal} modal={modal} setModal={setModal} />
                    </CardBody>
                </Card>


            </div>
            <div>
                <Card>
                    <CardBody>
                        <CardTitle className='text-info'>Question Two Solution</CardTitle>
                        <Question2 text={text} description={description}  entered={entered} setEntered={setEntered} show={show} setShow={setShow} />

                    </CardBody>
                </Card>
            </div>
        </div>
      </div>
        <div className="fixed-bottom text-center text-primary">
            salesRuby <span>&#169;</span> {new Date().getFullYear()}
        </div>
    </div>
  );
}

export default App;
