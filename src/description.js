export const description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\n" +
    "\n" +
    "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n" +
    "\n" +
    "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";
export const optionalText =' New Delhi (CNN)In 1958, Shyamala Gopalan arrived in Berkeley, California, after traveling thousands of miles from her family to pursue a doctorate in nutrition and endocrinology.\n' +
    '\n' +
    '              Gopalan was a precocious 19-year-old student. She had already graduated early from the University of Delhi, but the trip to California marked her first time out of India, where her parents and three siblings lived.\n' +
    '              She was alone.\n' +
    '              Fortunately for Gopalan, she had chosen to study at a campus that was about to become the counterculture capital of the United States. There, she found a home within the Bay Area\'s vibrant Black community, which welcomed her with open arms.\n' +
    '              Gopalan became an active civil rights crusader, while she undertook her studies. She met her first love in the movement, a Jamaican economics student named Donald Harris. They married and had two daughters together, Maya and her older sister, Kamala, who was announced Tuesday as the presumptive Democratic nominee for vice president.\n' +
    '              "From almost the moment she arrived from India, she chose and was welcomed to the Black community," Harris wrote of her mother in her 2019 autobiography, "The Truths We Hold."\n' +
    '              "In a country where she had no family, they were her family -- and she was theirs."\n' +
    '              A historic pick: Read more\n' +
    '              Kamala Harris has spent her career breaking barriers\n' +
    '              Kamala Harris\' balancing act\n' +
    '              Kamala Harris\' Indian roots and why they matter\n' +
    '              Gopalan and Donald Harris divorced when the children were young, but she would continue to be active in the civil rights movement. Kamala Harris wrote that her mother was acutely aware she was raising two girls that the general public would assume were Black, not Black and Indian.\n' +
    '              Harris credits her mother, who died in 2009, as one of her most important influences in her life who, along with others, inspired her to go into politics.\n' +
    '              But while Gopalan\'s sense of civic duty may have found new purpose in Berkeley, it was forged in India.\n' +
    '              Gopalan\'s mother and Harris\' grandmother, Rajam Gopalam, was an outspoken community organizer. Rajam\'s husband, P.V. Gopalam, was an accomplished Indian diplomat.\n' +
    '              "My mother had been raised in a household where political activism and civic leadership came naturally," Harris wrote in her book.\n' +
    '              "From both of my grandparents, my mother developed a keen political consciousness. She was conscious of history, conscious of struggle, conscious of inequities. She was born with a sense of justice imprinted on her soul."\n' +
    '              The influential grandfather\n' +
    '              That sense of justice was shaped in large part by P.V. Gopalan, who as a diplomat worked to help resettle refugees from East Pakistan -- modern-day Bangladesh -- in India after the country\'s partition, according to Harris\' maternal uncle, Gopalan Balachandran.\n' +
    '              Balachandran told CNN in a phone call that his father had strong views on humanitarian issues, which influenced Shyamala\'s upbringing.\n' +
    '              But that wasn\'t exactly what the two siblings bonded over when they were younger.\n' +
    '              Balachandran, 80, said he best remembers how he and his sister loved to play pranks and would get into trouble when they were younger and living in Mumbai. He remembers his father as stingy with advice and quiet but supportive.\n' +
    '              Gopalan\'s confidence in his children proved crucial when it came time for Shyamala to move to Berkeley. Balachandran said at the time, she would have been one of the first 19-year-old single Indian woman to travel to the US to study because of conservative attitudes about the role of women in India.\n' +
    '              But P.V. and Rajam Gopalan were progressive for their time. Balachandran said they offered to pay for the first year, and after that, Shyamala would have to make it on her own, which she did.\n' +
    '              "We were so happy," Balachandran said.\n' +
    '              Balachandran said his father was a bit warmer with his grandchildren, something Harris seems to reflect in her public comments about him.\n' +
    '              When they asked him for counsel, P.V. Gopalan would tell his grandchildren, "I will give you advice, but do what you think is best, what you like most, and do it well," Balachandran recalled.\n' +
    '              Harris called her grandfather one of her "favorite people in the world," in an interview with Los Angeles Times last year, while she was still campaigning for the democratic presidential nomination.\n' +
    '              Speaking in a 2009 interview with Aziz Haniffa, the former executive editor and a chief correspondent of India Abroad, Harris said some of her fondest childhood memories were walking along the beach with her retired grandfather when he lived in the southern Indian city of Chennai, formerly known as Madras.\n' +
    '              "He would take walks every morning along the beach with his buddies who were all retired government officials and they would talk about politics, about how corruption must be fought and about justice," Harris said. "They would laugh and voice opinions and argue, and those conversations, even more than their actions, had such a strong influence on me in terms of learning to be responsible, to be honest, and to have integrity."\n' +
    '              Harris said her grandfather was one of the "original independence fighters in India," but her uncle downplayed P.V.\'s role in India\'s fight against the British.\n' +
    '              \'Make Shyamala proud\'\n' +
    '              Harris\' aunt, Sarala Gopalan, was awoken at 4 a.m. on Wednesday in Chennai with the news that her niece was former Vice President Joe Biden\'s pick to join her on the Democratic ticket.\n' +
    '              She didn\'t go back to sleep.\n' +
    '              "The family are all very happy, all of us," she told CNN affiliate CNN News 18.\n' +
    '              Balachandran wasn\'t exactly surprised. He knows US politics, both from his time in the country -- he obtained a doctorate in economics and computer science from the University of Wisconsin -- and his work as a regular commentator for The Hindu, one of India\'s most prominent English-language newspapers.\n' +
    '              Once Biden said he was going to nominate a woman, Balachandran thought it was "very, very likely" it would be Harris based on her experience and background\n' +
    '              Balachandran said he and Harris don\'t speak that often, in large part due the distance and the demands of being a high-ranking US politician.\n' +
    '              He joked that people in India who call Harris a "female Barack Obama" should now be calling the 44th US President a "male Kamala Harris."\n' +
    '              When asked if he had a message for his niece, Balachandran remembered something his sister used to say.\n' +
    '              "Shyamala always said never sit still. If you can do something, do something," he said.\n' +
    '              "Make Shyamala proud."';