import React, { useState } from 'react';
import {
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText
} from 'reactstrap';

const TopBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand className='text-primary' href="/">SalesRuby</NavbarBrand>
          <NavbarToggler onClick={toggle} />
            <Nav className="mr-auto" navbar>
              <NavItem>
                <NavLink className='text-warning' href="https://www.linkedin.com/in/julius-jackson-aaa0b0176/" target={'blank'}>LinkedIn</NavLink>
              </NavItem>
            </Nav>
            <NavbarText className='text-success'>
              Julius Ossim Answers
            </NavbarText>
        </Navbar>
      </div>
  );
}

export default TopBar;