import React from 'react';
import {Table, Button} from 'reactstrap'
import './App.css';
import { Tooltip } from 'reactstrap';
const Question2 = props => {
const toggle = ()=> props.setEntered(!props.entered);
  return (
    <div>
      <Table striped bordered hover>
        <thead>
        <tr>
          <th>s/n</th>
          <th> name</th>
          <th>description</th>
          <th>date</th>
          <th>action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>1</td>
          <td>Julius</td>
          <td className='desc'>
            <div id="Tooltip">
              {!props.show && (<p>{props.text}...</p>)}
              {props.show && (<p>{props.description}</p>)}
            </div>
            <Tooltip placement="bottom" isOpen={props.entered} target="Tooltip" toggle={toggle}>
              {props.description}
            </Tooltip>
          </td>
          <td>@August, 12, 2020</td>
          <td>
            <Button color={props.show? 'danger':'info'} onClick={()=>props.setShow(!props.show)}>
              {props.show? 'Exit': 'Show'}
            </Button>

          </td>
        </tr>

        </tbody>
      </Table>
    </div>
  );
}

export default Question2;
