/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {optionalText} from './description'
import './App.css';

const Question1 = (props) => {
  const toggle = () => props.setModal(!props.modal);

  return (
      <div>
        <Modal animation='false' backdrop='static' isOpen={props.modal} toggle={toggle} className='modal-dimension slide-in-left'>
          <ModalHeader toggle={toggle}>Question One</ModalHeader>
          <ModalBody className='modal-dimension'>
              {optionalText}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={toggle}>close</Button>
          </ModalFooter>
        </Modal>
      </div>
  );
}

export default Question1;